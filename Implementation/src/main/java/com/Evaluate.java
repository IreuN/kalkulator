package com;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Evaluate implements Calculator {
    public double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                if (++pos < str.length()) {
                    ch = str.charAt(pos);
                } else {
                    ch = -1;
                }
            }

            boolean take_char(int charToTake) {
                while (ch == ' ') nextChar(); // Pomijamy spacje
                if (ch == charToTake) { // Gdy dotrzemy do poszkiwanego znaku
                    nextChar(); // Pobieramy go i zwracamy true
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected character: " + (char) ch);
                return x;
            }

            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (take_char('+')) x += parseTerm(); // Dodawanie
                    else if (take_char('-')) x -= parseTerm(); // Odejmowanie
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseSign();
                for (; ; ) {
                    if (take_char('*')) { // Mnożenie
                        x *= parseSign();
                    } else if (take_char('/')) { // Dzielenie
                        x /= parseSign();
                    } else {
                        return x;
                    }
                }
            }

            double parseSign() {
                if (take_char('+')) return parseSign(); // Liczby dodatnie
                if (take_char('-')) return -parseSign(); // Liczby ujemne

                double x;
                int startPos = this.pos;
                if (take_char('(')) { // parentheses
                    x = parseExpression();
                    take_char(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseSign();

                    int Found = 0;
                    for (Method metoda : Common.Reference.ReturnMethod()) {
                        if (metoda.getName().equals(func)) {
                            Object funcObject = Common.Reference.ReturnObject();
                            Found = 1;
                            try {
                                x = (double) metoda.invoke(funcObject, x);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                    }

                    if (Found == 0) {
                        Logger logger = Logger.getLogger(Evaluate.class);
                        logger.error("Unknown function: " + func);
                        throw new RuntimeException("Unknown function: " + func);
                    }

                } else {
                    throw new RuntimeException("Unexpected character: " + (char) ch);
                }

                if (take_char('^')) x = Math.pow(x, parseSign());

                return x;
            }
        }.parse();
    }
}