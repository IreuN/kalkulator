package com;

import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    private static Logger logger = Logger.getLogger(Evaluate.class);

    public static void main(String[] args) {

        String path = "C:\\Users\\gatko\\IdeaProjects\\Kalkulator\\CalcPlugin\\target\\CalcPlugin-0.1.jar"; // Sciezka do pliku jar
        logger.debug(path);

        URL pathURL = null; // Zamieniamy na URL
        try {
            pathURL = Paths.get(path).toUri().toURL();
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        }
        URL[] pathURLtab = new URL[]{pathURL}; // Zamieniamy na tablicę

        // Tworzymy nowy classloader o danej ścieżce
        ClassLoader cl = new URLClassLoader(pathURLtab);

        Class<?> calcs = null; // Ładujemy klasę calcs z pliku jar
        try {
            calcs = cl.loadClass("calcs");
        } catch (ClassNotFoundException e) {
            logger.error("Class not found. " + e.getMessage());
        }
        // Create a new instance from the loaded class
        Constructor<?> constructor = null; // Wyciągamy kontrusktor
        try {
            constructor = calcs.getConstructor();
        } catch (NoSuchMethodException e) {
            logger.error("No Such Method " + e.getMessage());
        }
        Object myClassObject = null; // Tworzymy nową instancję
        try {
            myClassObject = constructor.newInstance();
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
            logger.error(e.getMessage());
        }

        Method[] calcsmethods = calcs.getMethods(); // Pobieramy tablicę metod
        Common.Reference.Add(myClassObject, calcsmethods); // Przekazujemy do Reference obiekt i metody

        Scanner wej = new Scanner(System.in);   // wczytywanie zmiennych
        System.out.println("Podaj działanie do obliczenia:");
        String expression = wej.nextLine();

        Evaluate EvalMethod = new Evaluate();

        System.out.print("Wynik to: ");

        try {
            System.out.print(EvalMethod.eval(expression));
        } catch (RuntimeException RE) {
            logger.error(RE.getMessage());
        }

    }
}