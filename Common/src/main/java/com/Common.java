package com;

import java.lang.reflect.Method;

public class Common {
    private static Object ObjectField;
    private static Method[] MethodsField;

    static class Reference {
        public static void Add(Object ObjectArg, Method[] arg) {
            MethodsField = arg;
            ObjectField = ObjectArg;
        }

        public static Method[] ReturnMethod() {
            return MethodsField;
        }

        public static Object ReturnObject() {
            return ObjectField;
        }
    }
}
