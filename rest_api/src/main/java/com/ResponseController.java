package com;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseController {
    private double Evaluation;

    @GetMapping("/")
    public Response Evaluate(@RequestParam(value = "expression", defaultValue = "") String Expression) {
        Evaluate EvalMethod = new Evaluate();
        Evaluation = EvalMethod.eval(Expression);
        return new Response(Expression, Double.toString(Evaluation));
    }
}
