package com;

public class Response {
    private final String expression;
    private final String evaluation;

    public Response(String expression, String evaluation) {
        this.expression = expression;
        this.evaluation = evaluation;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public String getExpression() {
        return expression;
    }
}
